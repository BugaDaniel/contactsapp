package com.example.dan.uselistview.core;

import java.io.Serializable;

public class Person {
    private String fullName;
    private String phone;

    Person(String fullName, String phone){
        this.fullName = fullName;
        this.phone = phone;
    }

    public static class Builder{
        public static Person newPerson(String fullName,String phone){
            if(checkValidString(fullName) && checkValidString(phone)) {
                return new Person(fullName, phone);
            }
            return null;
        }
    }

    private static boolean  checkValidString(String personInfo){
        if(personInfo.equals("")){
            return false;
        }
        return true;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setFullName(String fullName){
        this.fullName=fullName;
    }

    public void setPhone(String phone){
        this.phone=phone;
    }
}
