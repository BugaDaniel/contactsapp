package com.example.dan.uselistview.interfaces;

import com.example.dan.uselistview.core.Person;

import java.util.ArrayList;

public interface StorageInterface {
    void addPerson(Person addedPerson);
    void deletePerson(int index);
    void updatePerson(int index, String name, String phone);
    ArrayList<Person> getPersonList();
}
