package com.example.dan.uselistview.storages;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.dan.uselistview.ContactsApplication;
import com.example.dan.uselistview.core.Person;
import com.example.dan.uselistview.interfaces.StorageInterface;
import com.google.gson.Gson;

import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;

import static android.app.PendingIntent.getActivity;
import static android.content.Context.MODE_PRIVATE;

public class SharedPrefStorage implements StorageInterface {
    private Context context;
    public SharedPrefStorage(Context context){
        this.context = context;
    }
    private List<Person> personsList = ContactsApplication.getPersonList();

    String personsJSONList = new Gson().toJson(personsList);
    SharedPreferences prefs = context.getSharedPreferences("contacts", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = prefs.edit();

    /*editor.putString("contactsListString", );
    editor.apply();*/

    @Override
    public void addPerson(Person addedPerson) {
        personsList.add(addedPerson);
    }

    @Override
    public void deletePerson(int index) {
        personsList.remove(index);
    }

    @Override
    public void updatePerson(int index, String name,String phone) {
        Person tempPers = personsList.get(index);
        tempPers.setFullName(name);
        tempPers.setPhone(phone);
    }

    @Override
    public ArrayList<Person> getPersonList() {
        return personsList;
    }


}
