package com.example.dan.uselistview.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dan.uselistview.ContactsApplication;
import com.example.dan.uselistview.R;
import com.example.dan.uselistview.core.CustomAdapter;
import com.example.dan.uselistview.core.Person;
import com.example.dan.uselistview.interfaces.StorageInterface;
import com.example.dan.uselistview.storages.SharedPrefStorage;

public class ContactActivity extends AppCompatActivity {
    EditText phoneText;
    EditText nameText;
    Button callButton;
    Button editButton;
    Button saveButton;

    public int objectPosition;
    public boolean isInViewMode;
    public Person selectedPerson;

    StorageInterface prefStorage = new SharedPrefStorage(getApplicationContext());



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_contact_info);

        final Intent intent = getIntent();
        objectPosition = (Integer)intent.getIntExtra("objectPosition", -1);
        isInViewMode = (boolean) intent.getBooleanExtra("isAddButtonPressed", false);

        nameText = (EditText) findViewById(R.id.person_name);
        phoneText = (EditText) findViewById(R.id.person_phone);
        saveButton = (Button) findViewById(R.id.saveButton);
        callButton = (Button) findViewById(R.id.callButton);
        editButton = (Button) findViewById(R.id.editButton)   ;

        if(!isInViewMode){
            editMode();
            saveButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    String name = nameText.getText().toString();
                    String phone = phoneText.getText().toString();
                    if(selectedPerson !=null){
                        selectedPerson.setPhone(phone);
                        selectedPerson.setFullName(name);

                        Toast.makeText(getApplicationContext(), "Saved successful",
                                Toast.LENGTH_LONG).show();

                    }
                    else{
                        Person newPerson = Person.Builder.newPerson(name, phone);
                        if(newPerson == null){
                            Toast.makeText(getApplicationContext(), "INVALID CONTACT",
                                    Toast.LENGTH_LONG).show();
                            return;
                        }else {
                            ContactsApplication.addPerson(newPerson);
                        }
                    }

                    CustomAdapter adapter = ContactsApplication.getCustomAdapter();
                    adapter.notifyDataSetChanged();
                    viewMode();
                }
            });

        }
        else{

            viewMode();

            if(objectPosition != -1){
                selectedPerson = ContactsApplication.getPersonByPosition(objectPosition);
                final String phoneNr = selectedPerson.getPhone();
                phoneText.setText(phoneNr);
                final String personName = selectedPerson.getFullName();
                nameText.setText(personName);
            }

            callButton = (Button) findViewById(R.id.callButton);
            callButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {

                    Intent callIntent;
                    if (ContextCompat.checkSelfPermission(ContactActivity.this, Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Permission is not granted
                        callIntent = new Intent(Intent.ACTION_DIAL);
                    }else {
                        callIntent = new Intent(Intent.ACTION_CALL);
                    }

                    String phoneForIntent = "tel:" + selectedPerson.getPhone();
                    callIntent.setData(Uri.parse(phoneForIntent));
                    startActivity(callIntent);
                }
            });

            editButton = (Button) findViewById(R.id.editButton);
            editButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    editMode();
                }
            });
        }

    }

    private void viewMode(){
        isInViewMode = true;
        nameText.setEnabled(false);
        phoneText.setEnabled(false);
        callButton.setVisibility(View.VISIBLE);
        editButton.setVisibility(View.VISIBLE);
        saveButton.setVisibility(View.GONE);
    }

    private void editMode(){
        isInViewMode = false;
        nameText.setEnabled(true);
        phoneText.setEnabled(true);
        callButton.setVisibility(View.GONE);
        editButton.setVisibility(View.GONE);
        saveButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if(isInViewMode || objectPosition == -1) {
            super.onBackPressed();
        }else {
            viewMode();
        }
    }
}
