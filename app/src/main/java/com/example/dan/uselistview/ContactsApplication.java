package com.example.dan.uselistview;

import android.app.Application;

import com.example.dan.uselistview.core.CustomAdapter;
import com.example.dan.uselistview.core.Person;

import java.util.ArrayList;

public class ContactsApplication extends Application {
    private static ArrayList<Person> persons = new ArrayList<>();
    public static CustomAdapter customAdapter;

    public static void setCustomAdapter(CustomAdapter adapter){
        customAdapter=adapter;
    }

    public static CustomAdapter getCustomAdapter() {
        return customAdapter;
    }

    public static ArrayList<Person> getPersonList(){
        return persons;
    }

    public static Person getPersonByPosition(int position){
        return persons.get(position);
    }

    public static void removePerson(int position){
        persons.remove(position);
    }

    public static void addPerson(Person person){
        persons.add(person);
    }
}
