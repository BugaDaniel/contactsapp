package com.example.dan.uselistview.core;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;


import com.example.dan.uselistview.R;

import java.util.ArrayList;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class CustomAdapter extends BaseAdapter {

    Context context;
    ArrayList<Person> persons;
    LayoutInflater inflter;

    TextView txtV;


    public CustomAdapter(Context applicationContext, ArrayList<Person> persons){
        this.context = applicationContext;
        this.persons = persons;
        inflter = (LayoutInflater.from(applicationContext));
    }


    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int position) {
        return persons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.single_row, null);
        txtV = (TextView) convertView.findViewById(R.id.text_view);
        String temp = persons.get(position).getFullName();
        txtV.setText(temp);
        return convertView;
    }

}


