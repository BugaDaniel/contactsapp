package com.example.dan.uselistview.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.dan.uselistview.ContactsApplication;
import com.example.dan.uselistview.R;
import com.example.dan.uselistview.core.CustomAdapter;
import com.example.dan.uselistview.core.Person;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Person person1 = Person.Builder.newPerson("Sergiu Alexandrescu", "074222222");
    private Person person2 = Person.Builder.newPerson("Dan Buga", "0745084687");
    private static ArrayList<Person> persons = (ArrayList<Person>) ContactsApplication.getPersonList();
    private ListView simpleList;
    private Button addButton;
    CustomAdapter customAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE},
                1);

        persons.add(person1);
        persons.add(person2);
        simpleList = (ListView) findViewById(R.id.list_view);
        customAdapter = new CustomAdapter(getApplicationContext(), ContactsApplication.getPersonList());
        ContactsApplication.setCustomAdapter(customAdapter);
        simpleList.setAdapter(customAdapter);

        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                intent.putExtra("objectPosition", position);
                intent.putExtra("isAddButtonPressed", true);
                startActivity(intent);
            }
        });
        simpleList.setLongClickable(true);
        simpleList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> a, View v, int position, long id) {

                AlertDialog.Builder adb=new AlertDialog.Builder(MainActivity.this);
                adb.setTitle("Delete?");
                adb.setMessage("Are you sure you want to delete contact?");
                final int positionToRemove = position;
                adb.setNegativeButton("Cancel", null);
                adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // here i will remove from persistent data
                        ContactsApplication.removePerson(positionToRemove);
                        customAdapter.notifyDataSetChanged();
                    }});
                adb.show();

                return true;
            }
        });

        addButton = (Button) findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
               Intent addContactIntent = new Intent(MainActivity.this, ContactActivity.class);
               boolean addButtonPressed = true;
               addContactIntent.putExtra("objectPosition", -1);
               //addContactIntent.putExtra("isAddButtonPressed", addButtonPressed);
               startActivity(addContactIntent);
               
            }
        });
    }


}
